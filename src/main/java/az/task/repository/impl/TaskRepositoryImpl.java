package az.task.repository.impl;

import az.task.domain.Task;
import az.task.util.ColumnType;
import az.task.util.TaskRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import az.task.repository.TaskRepository;

import java.util.List;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private TaskRowMapper taskRowMapper;

    @Override
    public long getTotalCount() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        return jdbcTemplate.queryForObject(SqlQuery.GET_TOTAL_COUNT,params,Long.class);
    }

    @Override
    public long getSearchCount(String search) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("searchWord","%"+search+"%");

        return jdbcTemplate.queryForObject(SqlQuery.GET_SEARCH_COUNT,params,Long.class);
    }

    @Override
    public List<Task> getTaskData(String search, Long column, String columnDir, Long start, Long length) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("searchWord","%"+search+"%");
        String sql = String.format(SqlQuery.GET_DATA, ColumnType.getColType(column),columnDir,start,length);
        return jdbcTemplate.query(sql,params,taskRowMapper);
    }

    @Override
    public List<Task> getTaskById(long id) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        return jdbcTemplate.query(SqlQuery.GET_TASK_BY_ID,params,taskRowMapper);
    }

    @Override
    public void deleteTaskById(long id) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        int count = jdbcTemplate.update(SqlQuery.DELETE_TASK_BY_ID,params);
        if (count > 0) {
            System.out.println(count + " Task Deleted");
        }else {
            throw new RuntimeException("Task can not delete");
        }
    }

    @Override
    public void addtask(Task task) {
       // System.out.println("TAsk get ABout: " + task.getAbout());
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name",task.getName());
        params.addValue("assign",task.getAssign());
        params.addValue("create_date",task.getCreateDate());
        params.addValue("finish_date",task.getFinishDate());
        params.addValue("about",task.getAbout());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(SqlQuery.ADD_TASK,params,keyHolder);

        if (count > 0) {
            long id = keyHolder.getKey().longValue();
            System.out.println(id + " added DB");
        }


    }

    /*
    " update task " +
            " set name=:name,assign=:assign,create_date=:create_date,finish_date=:finish_date,about=:about " +
            "where id = :id ";
     */
    @Override
    public void editTaskById(Task task) {

        System.out.println("TAsk get ABout: " + task.getAbout());
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",task.getId());
        params.addValue("name",task.getName());
        params.addValue("assign",task.getAssign());
        params.addValue("create_date",task.getCreateDate());
        params.addValue("finish_date",task.getFinishDate());
        params.addValue("about",task.getAbout());

        int count = jdbcTemplate.update(SqlQuery.EDIT_TASK_BY_ID,params);
        if (count > 0) {
            System.out.println(count + " task updated");
        }else {
            throw new RuntimeException("Update Error");
        }
    }
}
