package az.task.repository.impl;

public class SqlQuery {
    public static final String GET_TOTAL_COUNT = "select count(id) as count from task";

    public static final String GET_SEARCH_COUNT = "select count(id) from task " +
            "where concat(id,name,assign,create_date,finish_date,about) like :searchWord";

    public static final String GET_DATA = "select id,name,assign,create_date,finish_date,about from task " +
            "where concat(id,name,assign,create_date,finish_date,about) like :searchWord " +
            "order by %s %s " +
            "limit %d, %d ";

    public static final String GET_TASK_BY_ID = "select id,name,assign,create_date,finish_date,about from task " +
            "where id = :id ";

    public static final String DELETE_TASK_BY_ID = "delete from task " +
            "where id = :id ";

    public static final String ADD_TASK = "insert into task(id,name,assign,create_date,finish_date,about) " +
            "values (null,:name,:assign,:create_date,:finish_date,:about) ";

    public static final String EDIT_TASK_BY_ID = " update task " +
            " set name=:name,assign=:assign,create_date=:create_date,finish_date=:finish_date,about=:about " +
            "where id = :id ";
}
