package az.task.repository;

import az.task.domain.Task;

import java.util.List;

public interface TaskRepository {
    long getTotalCount();
    long getSearchCount(String search);
    List<Task> getTaskData(String search, Long column, String columnDir, Long start, Long length);
    List<Task> getTaskById(long id);
    void deleteTaskById(long id);
    void addtask(Task task);
    void editTaskById(Task task);
}
