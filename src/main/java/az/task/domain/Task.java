package az.task.domain;

import java.time.LocalDate;

public class Task {
    private long id;
    private String name;
    private String assign;
    private LocalDate createDate;
    private LocalDate finishDate;
    private String about;

    public Task() {
        this.id = 0;
        this.name = "";
        this.assign = "";
        this.createDate = null;
        this.finishDate = null;
        this.about = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", assign='" + assign + '\'' +
                ", createDate=" + createDate +
                ", finishDate=" + finishDate +
                ", about='" + about + '\'' +
                '}';
    }
}
