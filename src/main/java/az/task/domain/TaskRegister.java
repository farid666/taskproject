package az.task.domain;

public class TaskRegister {
    private long id;
    private String name;
    private String assign;
    private String createDate;
    private String finishDate;
    private String about;

    public TaskRegister() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    @Override
    public String toString() {
        return "TaskRegister{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", assign='" + assign + '\'' +
                ", createDate='" + createDate + '\'' +
                ", finishDate='" + finishDate + '\'' +
                ", about='" + about + '\'' +
                '}';
    }
}
