package az.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AztaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(AztaskApplication.class, args);
	}

}
