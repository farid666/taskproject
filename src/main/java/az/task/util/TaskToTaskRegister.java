package az.task.util;

import az.task.domain.Task;
import az.task.domain.TaskRegister;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
public class TaskToTaskRegister {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public TaskRegister getTaskRegister(Task task){
        TaskRegister taskRegister = new TaskRegister();
        taskRegister.setId(task.getId());
        taskRegister.setName(task.getName());
        taskRegister.setAssign(task.getAssign());
        taskRegister.setCreateDate(dateTimeFormatter.format(task.getCreateDate()));
        taskRegister.setFinishDate(dateTimeFormatter.format(task.getFinishDate()));
        taskRegister.setAbout(task.getAbout());
        return taskRegister;
    }

}
