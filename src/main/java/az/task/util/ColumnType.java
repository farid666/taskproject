package az.task.util;

public enum ColumnType {
    Id(0),
    Name(1),
    Assign(2),
    Create_Date(3),
    Finish_Date(4),
    About(5);

    private int value;

    ColumnType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ColumnType getColType(long value) {
        ColumnType columnType = null;
        if (Id.value == value) {
            columnType = Id;
        }else if (Name.value == value) {
            columnType = Name;
        }else if (Assign.value == value) {
            columnType = Assign;
        }else if (Create_Date.value == value) {
            columnType = Create_Date;
        }else if (Finish_Date.value == value) {
            columnType = Finish_Date;
        }else if (About.value == value) {
            columnType = About;
        }
        return columnType;
    }
}
