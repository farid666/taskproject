package az.task.util;

import az.task.domain.Task;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TaskRowMapper implements RowMapper<Task> {
    @Override
    public Task mapRow(ResultSet resultSet, int i) throws SQLException {
        Task task = new Task();
        task.setId(resultSet.getLong("id"));
        task.setName(resultSet.getString("name"));
        task.setAssign(resultSet.getString("assign"));
        task.setCreateDate(resultSet.getDate("create_date").toLocalDate());
        task.setFinishDate(resultSet.getDate("finish_date").toLocalDate());
        task.setAbout(resultSet.getString("about"));
        return task;
    }
}
