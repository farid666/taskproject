package az.task.util;

import az.task.domain.Task;
import az.task.domain.TaskRegister;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class TaskRegisterToTask {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public Task getTask(TaskRegister taskRegister){
        Task task = new Task();
        task.setId(taskRegister.getId());
        task.setName(taskRegister.getName());
        task.setAssign(taskRegister.getAssign());
        task.setCreateDate(LocalDate.parse(taskRegister.getCreateDate(),formatter));
        task.setFinishDate(LocalDate.parse(taskRegister.getFinishDate(),formatter));
        task.setAbout(taskRegister.getAbout());
        return task;
    }


}
