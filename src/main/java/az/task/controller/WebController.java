package az.task.controller;


import az.task.domain.DataTableRequest;
import az.task.domain.DataTableResponse;
import az.task.domain.Task;
import az.task.domain.TaskRegister;
import az.task.service.TaskService;
import az.task.util.TaskRegisterToTask;
import az.task.util.TaskToTaskRegister;
import az.task.validator.TaskValidator;
import org.apache.commons.beanutils.MappedPropertyDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RequestMapping("/")
@Controller
public class WebController {
    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskValidator taskValidator;

    @Autowired
    private TaskRegisterToTask taskRegisterToTask;

    @Autowired
    private TaskToTaskRegister taskToTaskRegister;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();

        if (target != null) {
            if (target.getClass().equals(TaskRegister.class)) {
                binder.setValidator(taskValidator);
            }
        }
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/getAjaxTask")
    @ResponseBody
    public DataTableResponse getAjaxData(@RequestParam(name = "draw") long draw,
                                         @RequestParam(name = "start") long start,
                                         @RequestParam(name = "length") long length,
                                         @RequestParam(name = "order[0][column]") long sortColumn,
                                         @RequestParam(name = "order[0][dir]") String sortDirection,
                                         @RequestParam(name = "search[value]") String searchValue){
        DataTableRequest dataTableRequest = new DataTableRequest();
        dataTableRequest.setDraw(draw);
        dataTableRequest.setStart(start);
        dataTableRequest.setLength(length);
        dataTableRequest.setSortColumn(sortColumn);
        dataTableRequest.setSortDirection(sortDirection);
        dataTableRequest.setSearchValue(searchValue);

        return taskService.getDataTableResponse(dataTableRequest);

    }

    @GetMapping("/viewTask")
    public ModelAndView getTaskById(@RequestParam(name = "id") long id) {
        ModelAndView modelAndView = new ModelAndView();
        List<Task> list = taskService.getTaskById(id);

        if (!list.isEmpty()) {
            Task task = list.get(0);
            modelAndView.addObject("task",task);
            modelAndView.setViewName("taskView");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @GetMapping("/deleteTask")
    public String deleteTaskById(@RequestParam(name = "id") long id) {

        try {
            taskService.deleteTaskById(id);
            return "index";
        }catch (Exception e) {
            return "errorView";
        }
    }

    @GetMapping("/addTask")
    public ModelAndView addTask() {
        ModelAndView modelAndView = new ModelAndView();
        TaskRegister taskRegister = new TaskRegister();
        modelAndView.addObject("taskRegister",taskRegister);
        modelAndView.setViewName("addTask");
        return modelAndView;
    }

    @PostMapping("/addTask")
    public String addTask2(@ModelAttribute(name = "taskRegister") @Validated TaskRegister taskRegister,
                           BindingResult result) {
        if (result.hasErrors()) {
            return "addTask";
        }else {
            taskService.addtask(taskRegisterToTask.getTask(taskRegister));
            return "index";
        }
    }

    @GetMapping("/editTask")
    public ModelAndView editTask(@RequestParam(name = "id") long id) {
        ModelAndView modelAndView = new ModelAndView();

        List<Task> list = taskService.getTaskById(id);

        if (!list.isEmpty()) {
            TaskRegister taskRegister = taskToTaskRegister.getTaskRegister(list.get(0));
            modelAndView.addObject("editTasks",taskRegister);
            modelAndView.setViewName("editTask");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @PostMapping("/editTask")
    public String editTask2(@ModelAttribute(name = "editTasks") @Validated TaskRegister taskRegister,
                            BindingResult result,
                            @RequestParam(name = "id") long id) {

        if (!result.hasErrors()) {
            try {
                taskService.editTaskById(taskRegisterToTask.getTask(taskRegister));
                return "redirect:/";
            }catch (Exception e) {
                return "redirect:errorView";
            }
        }else {
            return "editTask";
        }
    }
}
