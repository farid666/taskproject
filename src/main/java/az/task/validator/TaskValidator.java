package az.task.validator;

import az.task.domain.TaskRegister;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class TaskValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(TaskRegister.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        TaskRegister taskRegister = (TaskRegister) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"id","can not null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","can not null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"assign","can not null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"createDate","can not null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"finishDate","can not null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"about","can not null");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        if (!errors.hasErrors()) {
            LocalDate createDate = LocalDate.parse(taskRegister.getCreateDate(),dateTimeFormatter);
            LocalDate finishDate = LocalDate.parse(taskRegister.getFinishDate(),dateTimeFormatter);


            if (!finishDate.isAfter(createDate)) {
                errors.rejectValue("createDate","Date invalid");
            }
        }
    }
}
