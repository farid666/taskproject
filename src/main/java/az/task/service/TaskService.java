package az.task.service;

import az.task.domain.DataTableRequest;
import az.task.domain.DataTableResponse;
import az.task.domain.Task;

import java.util.List;

public interface TaskService {
    DataTableResponse getDataTableResponse(DataTableRequest request);
    List<Task> getTaskById(long id);
    void deleteTaskById(long id);
    void addtask(Task task);
    void editTaskById(Task task);
}
