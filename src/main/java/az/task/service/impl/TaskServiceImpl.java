package az.task.service.impl;

import az.task.domain.DataTableRequest;
import az.task.domain.DataTableResponse;
import az.task.domain.Task;
import az.task.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import az.task.repository.TaskRepository;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public DataTableResponse getDataTableResponse(DataTableRequest request) {
        DataTableResponse response = new DataTableResponse();

        response.setDraw(request.getDraw());
        response.setRecordsTotal(taskRepository.getTotalCount());
        response.setRecordsFiltered(taskRepository.getSearchCount(request.getSearchValue()));
        List<Task> list = taskRepository.getTaskData(request.getSearchValue(),request.getSortColumn(),request.getSortDirection(),request.getStart(),request.getLength());
        response.setData(new Object[list.size()][7]);
        System.out.println(response);
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                response.getData()[i][0] = list.get(i).getId();
                response.getData()[i][1] = list.get(i).getName();
                response.getData()[i][2] = list.get(i).getAssign();
                response.getData()[i][3] = list.get(i).getCreateDate();
                response.getData()[i][4] = list.get(i).getFinishDate();
                response.getData()[i][5] = list.get(i).getAbout();
                response.getData()[i][6] = "<a href=\"viewTask?id="+list.get(i).getId()+"\">View</a>  "+
                        "<a href=\"deleteTask?id="+list.get(i).getId()+"\">Delete</a>  "+
                        "<a href=\"editTask?id="+list.get(i).getId()+"\">Edit</a>";

            }
        }
        return response;
    }

    @Override
    public List<Task> getTaskById(long id) {
        return taskRepository.getTaskById(id);
    }

    @Override
    public void deleteTaskById(long id) {
        taskRepository.deleteTaskById(id);
    }

    @Override
    public void addtask(Task task) {
        taskRepository.addtask(task);
    }

    @Override
    public void editTaskById(Task task) {
        taskRepository.editTaskById(task);
    }
}
