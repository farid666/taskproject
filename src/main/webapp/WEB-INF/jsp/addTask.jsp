<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 18.11.19
  Time: 02:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Add Task</title>
</head>
<body>

    <form:form modelAttribute="taskRegister" action="addTask" method="post">
        Name<form:input path="name"/><br>
            <form:errors path="name"/><br>
        Assign:<form:input path="assign"/><br>
            <form:errors path="assign"/><br>
        CreateDate:<form:input path="createDate"/><br>
            <form:errors path="createDate"/><br>
        FinishDate:<form:input path="finishDate"/><br>
            <form:errors path="finishDate"/><br>
        About:<form:textarea path="about"/><br>
            <form:errors path="about"/><br>
        <input type="submit" value="Submit"/><br>
    </form:form>
</body>
</html>
