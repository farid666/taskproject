<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 17.11.19
  Time: 20:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task List</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
</head>
<body>
<a href="addTask">ADD TASK</a><br>

<table id="taskTable">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Assign</th>
        <th>Create_Date</th>
        <th>Finish_Date</th>
        <th>About</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#taskTable').DataTable( {
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "processing": true,
            "serverSide": true,
            "ajax": "/getAjaxTask"
        } );
    } );
</script>



</body>
</html>
